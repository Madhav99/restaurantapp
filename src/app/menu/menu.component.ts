import { Component, OnInit, Inject } from '@angular/core';
import { DishService } from '../services/dish.service';
import { Dish } from '../shared/dish';
import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class MenuComponent implements OnInit {
 
  constructor(private dishService: DishService,
              @Inject('BaseURL') public baseURL) { }

  dishes: Dish[];
  errMess: string;
 
  ngOnInit(): void {
    console.log(new Date(), "menu");
    this.dishService.getDishes().subscribe(
      (dishes) => this.dishes = dishes,
      (errmess) => this.errMess = <any>errmess
      );
  }

}
