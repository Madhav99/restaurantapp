import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { expand, flyInOut, visibility } from '../animations/app.animation';
import { ActivatedRoute, Params } from '@angular/router';
import { DishService } from '../services/dish.service';
import { Dish } from '../shared/dish';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment } from '../shared/comment';


@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
  animations: [
   visibility(), 
   flyInOut(),
   expand()
  ]
})

export class DishdetailComponent implements OnInit {


  @ViewChild('cform') commentFormDirective;

  visibility = 'shown';

  formErrors = {
    'author': '',
    'review': '',
  };


  validationMessages = {
    'author': {
      'required': 'Author Name is required.',
      'minlength': 'Author Name must be at least 2 characters long.',
    },
    'review': {
      'required': 'Comment is required.',
    }
  };



  dish: Dish;
  dishIds: string[];
  errMess: string;
  prev: string;
  next: string;
  commentForm: FormGroup;
  comment: Comment;
  date =  new Date().toISOString().slice(0, 10);
  
  constructor(private route: ActivatedRoute, private dishservice: DishService, private fb: FormBuilder, 
              private location: Location, @Inject('BaseURL') public baseURL) { }

  ngOnInit(): void {
     console.log( new Date(), "dishdetail");
      this.dishservice.getDishIds().subscribe((dishIds) => this.dishIds = dishIds);
      this.route.params.pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); }))
      .subscribe(
        (dish) => { 
          this.dish = dish; 
          this.setPrevNext(dish.id);
          this.createForm(); 
          this.visibility = 'shown'; 
        },
        (errmess) => this.errMess = <any>errmess
      );
  }

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }


  createForm() {
    this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2)]],
      rating: '1',
      review: ['', [Validators.required]]
    });

    this.commentForm.valueChanges.subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); // (re)set validation messages now
  }


  onSubmit() {

    this.comment = new Comment();
    this.comment.rating = this.commentForm.value.rating;
    this.comment.comment = this.commentForm.value.review;
    this.comment.author = this.commentForm.value.author;
    this.comment.date = this.date;
    this.dish.comments.push(this.comment);
    this.dishservice.putDish(this.dish).subscribe(
      () => {},
      (errmess) => this.errMess = <any>errmess
    );

    this.commentFormDirective.resetForm();
    this.commentForm.reset({
      author: '',
      rating: '1',
      review: ''
    });
    this.comment = null;
  }

  onValueChanged(data?: any) {
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }


}
